POST PROMISCUOUS PIPELINES PUBLICATION
--------------------------------------------------------------------

> Suppose you went back to Ada Lovelace and asked her the difference between a script and a program.
> She'd probably look at you funny, then say something like:
> Well, a script is what you give the actors, but a program is what you give the audience.
> [*](http://www.perl.com/pub/2007/12/06/soto-11.html)

> "Computer" comes from the Latin "putare" which means both to think and to prune. 
> Virgil's Georgics - depictions of country life - speak of tidying vines by pruning (fingitque putando).
> [*](http://www.bbc.com/news/blogs-magazine-monitor-35428300)


This repository speculates on what a Promiscuous Publication could be. It followed from the worksession _Promiscuous Pipelines_ that took place in the summer of 2015 in Brussels [*](http://constantvzw.org/site/-Promiscuous-pipelines-.html). A group of artists, programmers and theorists worked together on imagining modularity and modality differently, knowing that software processes are inherently leaky and contextual. Using the collection of notes, recordings and image-files from the session as a startingpoint, the publication uncovers, makes explicit the process of following links and moving from one system/service to another. Multiple inputs, even ones that don't exist yet, create multiple outputs. Every produced document is a potential source in itself. The publication prefers sprawling, meandering and circuitious streams over coherence and predictability.

We used the traditional software workflow of a Makefile[*](http://www.gnu.org/software/make/manual/make.html) as a platform for speculative markup. It allowed for a hyper flexible constellation of multi-style mini-programs spread out over files and folders, media and meta-data.


Multiheaded vs. hybrid publication
----------------------------------------------------------------

A Promiscuous Publication is NOT like parallel, hybrid or cross-media publishing. It is a multiheaded and polycentric form of making public.It enjoys its various forms, and embraces their idiosyncracies. As a multiheaded publication it is NOT obsessed with providing uniform outcomes across diverse media. Having  many  centers, it can produce new focal points and obfuscate its origins. It explores playfully a spectral diversity with sibling outcomes and bastard children that operate out of sync or tune.

We are interested in breaking the chain of hierarchical and linear editing, and prefer distributed responsibility: too often a small team of editors is tasked to correct mistakes, deal with legal issues and make selections after the fact, in order to turn a collection of notes into a structured publication later. We are curious of creating pipelines that stir and create currents and behaviours spiraling outside of the continuity of predictable flows, where their modality switches as their inputs solidify, evaporate and gelatinize into diverse forms in reactions to conditions, while influencing and changing the others.

How can the making-public itself be an engaging experience, interactive but with erratic options? How could the process of iterating between known sources and diverse and unfamilair pipelines be made tangible?

With contributions from: Active Archives, Agustina Andreoletti, An Mertens, Anne Laforet, Bash, bolwerK, Christoph Haag, Dymaxion, Eleanor Saitta, Etherpash, Etherpad, Femke Snelting, Flickr, ffmpeg, Foam, Gijs de Heij, Gottfried Haider, ImageMagick, Marthe Van Dessel, Martino Morandi, Michael Murtaugh, Microraptor/GEGL, Nik Gaffney, OpenCV, Øyvind Kolås, Pandoc, Pierre Marchand, Python, QueerOS, Simon Yuill, Sophie Toupin, Sophie Toupin, Unitary Networking, Wænd, Wendy Van Wijnsberghe, YouTube, Zeljko Blace and many more people, tools and resources.

MANUAL
------------

* Download or clone the repository
* Open a terminal, and navigate into the folder that you have downloaded:

        `$ cd $HOME/name-of-folder`

* Now you can type for example:

        `$ make promiscuous`

* To find out what different things can be made with this file, type:
  
        `$ make what?

* Some of the functionalities have not been implemented yet, but others have:

        `$ make wordlist` (a clickable index to all notes in the collection, organised by word)
        `$ make selection` (generate html pages based on paragraphs that contain certain words or combinations of words)
        `$ make forget` (remove paragraphs that contain certain words or combinations of words from your current archive)
        `$ make poster` (create a poster from multiple images, in this case an on-line flickr archive)
    
* In docs/maketed/maketed.html you find documentation for:
    
        `$ make transcription` (transforming an audiofile into a textfile with the help of YouTube's closecaption function)

----

> It possessed many heads, the exact number of which varies according to the source.
> [*](https://en.wikipedia.org/wiki/Lernaean_Hydra)
