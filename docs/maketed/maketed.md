---
title: "Simon meets Tim meets TED"
---

Gathering materials
-----------------------

Use a image search engine to search for "Simon Yuill":

[https://duckduckgo.com/?q=simon+yuill&t=ffsb&iax=1&ia=images](https://duckduckgo.com/?q=simon+yuill&t=ffsb&iax=1&ia=images
)

![Duckduck go image search results for 'Simon Yuill'](maketed01.png)

Download the first result (in this case it's just right ;) Use the GIMP to do some cropping and voilà:

![](../images/simonsface.jpg)

Now, we pull a [particularly juicy TED appearance by a very animated Tim Berners-Lee](http://video.ted.com/talk/podcast/2009/None/TimBernersLee_2009-480p.mp4):

```bash
mkdir -p video
curl http://video.ted.com/talk/podcast/2009/None/TimBernersLee_2009-480p.mp4 \
  -o video/TimBernersLee_2009-480p.mp4
```

OpenCV face swapping
----------------------------

Now, with some help from OpenCV, we stitch simon's face onto TBL's body...

```bash
python scripts/facereplace.py \
    --video video/TimBernersLee_2009-480p.mp4 \
    --face images/simonsface.jpg \
    --cascadexml etc/haarcascades/haarcascade_frontalface_default.xml \
    --output video/ted_simon_videoonly.avi
```

In the meantime, the audio has been clipped and amplified using [Audacity](http://www.audacityteam.org/) (sorry [sox](http://sox.sourceforge.net/), some things are hard to fully automate).


Putting the pieces together
------------------------------

Then, to join the audio and video, a fusion of ffmpeg recipes: [Creating a video slideshot from images](https://trac.ffmpeg.org/wiki/Create%20a%20video%20slideshow%20from%20images) and [How to merge audio and video files in ffmpeg](http://superuser.com/questions/277642/how-to-merge-audio-and-video-file-in-ffmpeg) and [Repeat/loop Input Video with ffmpeg?](http://video.stackexchange.com/questions/12905/repeat-loop-input-video-with-ffmpeg).

In sum (and kind of ridiculously) it seems the best thing to do is to extend the video to be at least as long as the audio (turns out we need 4 copies of the video as Video is 16:16 long and the audio is 1:02:23) and then to join the two streams using ffmpeg's *shortest* option, it will cut the video to fit the audio duration.


Extending the length of a video four times
---------------------------------------------

Make a "playlist"...

```bash
file 'ted_simon.video.avi'
file 'ted_simon.video.avi'
file 'ted_simon.video.avi'
file 'ted_simon.video.avi'
```
And use the ffmpeg *concat* filter with the playlist as input...

```bash
ffmpeg -f concat -i playlist.txt -c copy ted_simon.videox4.avi
```

Combining video + audio, cutting to the shortest duration (the audio in this case)
-----------------------------------------------------------------------------

```bash
ffmpeg -i video/ted_simon.videox4.avi \
    -i audio/PP_2015_09_03_Simon_s_talk.amp.ogg \
    -c:v libx264 \
    -c:a aac \
    -strict experimental \
    -b:a 192k \
    -shortest \
    ted_simon.mp4
```

Uploading the video to YouTube
----------------------------------

![](youtube_upload.png)

![](youtube_upload_thumbnail.png)

If you're not happy with the automatically selected thumbnail, there's an option to upload a custom thumbnail. Interestingly, this feature requires that "your account needs to be verified and in good standing". (links: [Learn more](https://support.google.com/youtube/answer/72431?hl=en) and [verified](https://www.youtube.com/verify_phone_number))

![](youtube_upload_thumbnail02.png)

[https://www.youtube.com/watch?v=Q1C5OX3WnDA](https://www.youtube.com/watch?v=Q1C5OX3WnDA)

Getting automatic subtitles out of youtube
----------------------------------------------

We are interested in getting the automatic captions out of YouTube. But how is this feature performed?

* [Use automatic captioning](https://support.google.com/youtube/answer/6373554)
* [Add subtitles & closed captions](https://support.google.com/youtube/answer/2734796)
* [](https://support.google.com/youtube/answer/2734796?rd=1)
![](automatic_captioning.png)

![](automatic_captioning02.png)

![](automatic_captioning_troubleshooting.png)


Using [youtube-dl](http://rg3.github.io/youtube-dl/):


```bash
youtube-dl --write-auto-sub --skip-download "https://www.youtube.com/watch?v=Q1C5OX3WnDA"
```

BUT, so far...

```bash
[youtube] Q1C5OX3WnDA: Downloading webpage
[youtube] Q1C5OX3WnDA: Downloading video info webpage
[youtube] Q1C5OX3WnDA: Extracting video information
[youtube] Q1C5OX3WnDA: Looking for automatic captions
WARNING: Couldn't find automatic captions for Q1C5OX3WnDA
[youtube] Q1C5OX3WnDA: Downloading MPD manifest
```

Check formats:
youtube-dl -F "https://www.youtube.com/watch?v=Q1C5OX3WnDA"

```bash
[youtube] Q1C5OX3WnDA: Downloading webpage
[youtube] Q1C5OX3WnDA: Downloading video info webpage
[youtube] Q1C5OX3WnDA: Extracting video information
[youtube] Q1C5OX3WnDA: Downloading MPD manifest
[info] Available formats for Q1C5OX3WnDA:
format code  extension  resolution note
140          m4a        audio only DASH audio  131k , m4a_dash container, mp4a.40.2@128k (44100Hz), 56.74MiB
160          mp4        256x144    DASH video  127k , avc1.4d400c, 24fps, video only, 49.29MiB
133          mp4        426x240    DASH video  302k , avc1.4d4015, 24fps, video only, 108.99MiB
134          mp4        640x360    DASH video  562k , avc1.4d401e, 24fps, video only, 134.45MiB
135          mp4        854x480    DASH video 1087k , avc1.4d401e, 24fps, video only, 289.51MiB
17           3gp        176x144    small , mp4v.20.3,  mp4a.40.2@ 24k
36           3gp        320x180    small , mp4v.20.3,  mp4a.40.2
5            flv        426x240    small , h263, mp3  @ 64k
18           mp4        640x360    medium , avc1.42001E,  mp4a.40.2@ 96k (best)
```





