what?:
	# Listing all possible commands or "targets" in this Makefile, independent of whether they are actually implemented or not  :-) <3
	# Using this recipe ... http://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile
	@grep '^[^#[:space:]].*:' Makefile

promiscuous:
	# This command adulterates any (supposedly) unadulterated file, or result.
	# It could add files from random shady resources, publish in parallel to multiple platforms without asking permission, combine commands ad-hoc and at will.

flickr_h:
	# flickr_m downloads medium sized images from Flickr. The download url can be changed in scripts/flickripr.py
	@sleep 2
	# Flickr is a service provided by Yahoo.
	@sleep 2
	# By accessing and using Flickr, you accept and agree to be bound by the terms and provision of the Yahoo Terms of Service. Please read: https://policies.yahoo.com/us/en/yahoo/terms/utos/index.htm
	@sleep 10
	# Assuming you read and agreed.
	@sleep 2
	# Now downloading ...
	@sleep 2
	mkdir -p images
	python scripts/flickripr.py --dir images --download --size h

flickr_m:
	# flickr_m downloads medium sized images from Flickr. The download url can be changed in scripts/flickripr.py
	@sleep 2
	# Flickr is a service provided by Yahoo.
	@sleep 2
	# By accessing and using Flickr, you accept and agree to be bound by the terms and provision of the Yahoo Terms of Service. Please read: https://policies.yahoo.com/us/en/yahoo/terms/utos/index.htm
	@sleep 10
	# Assuming you read and agreed.
	@sleep 2
	# Now downloading ...
	@sleep 2
	mkdir -p images
	python scripts/flickripr.py --dir images --download --size m

flickr_urls:
	python scripts/flickripr.py --dir images --size h

audio/PP_2015_09_03_Simon_s_talk.ogg:
	mkdir -p audio
	curl http://pipelines.constantvzw.org/audio/PP_2015_09_03_Simon_s_talk.ogg -o audio/PP_2015_09_03_Simon_s_talk.ogg

etc/haarcascades/haarcascade_frontalface_default.xml:
	# Downloading statistical "frontalface" model for haar cascade face detection...
	sleep 3 
	mkdir -p etc/haarcascades
	curl -o etc/haarcascades/haarcascade_frontalface_default.xml

images/simonsface.jpg:
	#
	# Downloading Simons face ...
	#
	@sleep 3
	mkdir -p images
	curl http://pipelines.constantvzw.org/images/simonsface.jpg -o images/simonsface.jpg

video/TimBernersLee_2009-480p.mp4:
	#
	# Downloading Tim Berners-Lee's infamous linked open data TED talk from http://www.ted.com/ ...
	#
	@sleep 3
	mkdir -p video
	curl http://video.ted.com/talk/podcast/2009/None/TimBernersLee_2009-480p.mp4 \
	  -o video/TimBernersLee_2009-480p.mp4

video/ted_simon_videoonly.avi: images/simonsface.jpg video/TimBernersLee_2009-480p.mp4 etc/haarcascades/haarcascade_frontalface_default.xml
	# Replacing Tim berners lee head with Simon Yuill's
	@sleep 2
	# Warning: opencv for python required for this part ...
	# Use "pip install opencv" if available on your system
	@sleep 5
	python scripts/facereplace.py \
	    --video video/TimBernersLee_2009-480p.mp4 \
	    --face images/simonsface.jpg \
	    --cascadexml etc/haarcascades/haarcascade_frontalface_default.xml \
	    --output video/ted_simon_videoonly.avi

video/tedxsimon.avi: audio/PP_2015_09_03_Simon_s_talk.ogg video/ted_simon_videoonly.avi
	# Trying to use ffmpeg to merge together video with simon's audio
	@sleep 2
	ffmpeg -ss 02:00 -i audio/PP_2015_09_03_Simon_s_talk.ogg  \
	    -i video/ted_simon_videoonly.avi \
	    video/tedxsimon.avi

tedxsimon: video/tedxsimon.avi

screenplay:
	# This should produce a screenplay from any talk
	@sleep 2
	# It could re-format elements from transcription

re-enactment: screenplay
	# A narrator reads a screenplay
	@sleep 2
	# The recorded audio is added to a video file.

dump:
	# Download everything from a list of files on http://pad.constantvzw.org/p/pppp.list
	@sleep 2
	# Options could be added for various platforms, locations and file-types

%.thumb.jpg: %.jpg
	# Generating thumbnails
	convert $< -resize 120x $@

public:
	# Upload image files to wikimedia
	# This would need some kind of questionaire about licenses?

poster: flickr_m
	# Produce a poster from multiple images
	@sleep 2
	montage images/*_m.jpg poster.jpg || echo "If there was an error "[poster] Error 127", probably ImageMagick was not installed or something else went wrong. Sorry"
	# A file poster.jpg has been created in the folder "pppp"

collect:
	mkdir -p images
	mkdir -p audio
	mkdir -p video
	mkdir -p var
	scripts/collect.sh

wordlist:
	# Creating a word-index of all textfiles in the repository
	bash scripts/harvest_allwords.sh html/wordlist.html

selection:
    	# This allows you to select content from the repository based on words, combinations of words or even phrases.
	bash scripts/harvest_wordmatch.sh

forget:
	bash scripts/rm_wordmatch.sh
