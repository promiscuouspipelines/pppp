#/bin/bash


  PADBASEURL="http://pipelines.constantvzw.org/etherdump"
  ALLPADNAMES="http://pipelines.constantvzw.org/etherdump/allpads.txt"
  XFORMAT="txt"

  TMP="/tmp/pppp"
  DUMPHERE="${TMP}_allpads-with-name.dump"
   HTMLDIR="html"

  KEYWORD="$*"

# --------------------------------------------------------------------------- #
#  M A K E  S U R E  T H E R E  I S  S O M E T H I N G   T O   L O O K  F O R
# --------------------------------------------------------------------------- #
  if [ `echo $KEYWORD        | #
        sed 's/[^a-zA-Z]//g' | #
        wc -c` -lt 2 ]
   then
       clear
       echo "What should we look for?"; echo
       read -p "" KEYWORD
  fi
  if [ `echo $KEYWORD        | #
        sed 's/[^a-zA-Z]//g' | #
        wc -c` -lt 2 ]
   then
        echo "We need something to look for, please!"
        exit 
  fi

# --------------------------------------------------------------------------- #
#  C A R E   F O R   T H E   O U T P U T
# --------------------------------------------------------------------------- #

  HTMLOUT=$HTMLDIR/`echo $KEYWORD          | #
                    tr [:upper:] [:lower:] | #
                    sed 's/[^a-z]//g'`.html
  if [ -f $HTMLOUT ]; then
       echo "$HTMLOUT does exist"
       read -p "overwrite $HTMLOUT? [y/n] " ANSWER
    if [ X$ANSWER != Xy ] ; then echo "Bye"; exit; fi
  fi

# --------------------------------------------------------------------------- #
#  D U M P   A L L   P A D S   I N T O   O N E (IF NECESSARY)
# --------------------------------------------------------------------------- #
  if [ -f $DUMPHERE ]; then
      #echo "DUMP does exist ($DUMPHERE)"
       read -p "UPDATE DUMP FROM REMOTE? [y/n] " ANSWER
       if [ "X$ANSWER" == "Xy" ];
             then rm $DUMPHERE; 
       elif [ "X$ANSWER" != "Xn" ];
             then  echo "You should answer with y or n!"; exit;
       fi
  fi;  if [ ! -f $DUMPHERE ]; then
       for PAD in `wget --no-check-certificate -q -O - $ALLPADNAMES | # 
                   egrep -v "graph_meandering|references.bib" | # IGNORE THIS
                   sed "s,^,$PADBASEURL/," | sed "s,$,.$XFORMAT," `
        do
            echo $PAD
            PADNAME=`basename $PAD`
            wget --no-check-certificate -q -O - $PAD | #
            sed "s,^,${PADNAME}:," >> $DUMPHERE
       done
  fi

# --------------------------------------------------------------------------- #
#  L O O K   F O R   K E Y W O R D 
# --------------------------------------------------------------------------- #
  if [ `grep -i "\b$KEYWORD\b" $DUMPHERE | # FIND KEY
        wc -l` -gt 0 ]; then

  echo "<html><meta charset=\"UTF-8\"/><body>"   >  $HTMLOUT

 ( IFS=$'\n'
  for LN in `grep -n "" $DUMPHERE   | # NUMBER ALL LINES
             grep -i "\b$KEYWORD\b" | # FIND KEY
             cut -d ":" -f 1`         # EXTRACT LINE NUMBER
   do
        PREVEMPTYLINE=`grep -n "" $DUMPHERE          | # NUMBER ALL LINES
                       head -n ${LN}                 | # PRINT FROM START TO LINE NUM
                       sed '/^[0-9]*:.*txt:[ \t]*$/s/^/X/' | # MARK EMPTY LINES
                       grep "^X"                     | # SELECT MARKED
                       tail -n 1                     | # SELECT LAST
                       sed 's/^X//'                  | # REMOVE MARK
                       cut -d ":" -f 1`                # EXTRACT LINE NUMBER
        NEXTEMPTYLINE=`grep -n "" $DUMPHERE          | # NUMBER ALL LINES
                       sed "1,${LN}d"                | # PRINT FROM LINE NUM TO END
                       sed '/^[0-9]*:.*txt:[ \t]*$/s/^/X/' | # MARK EMPTY LINES
                       grep "^X"                     | # SELECT MARKED
                       head -n 1                     | # SELECT FIRST
                       sed 's/^X//'                  | # REMOVE MARK
                       cut -d ":" -f 1`                # EXTRACT LINE NUMBER

        if [ X$PREVEMPTYLINE != X$LASTMATCH ]; then

        MATCHNAME=`sed -n "${PREVEMPTYLINE}p" $DUMPHERE | cut -d ":" -f 1`
        MATCHURL=`echo "$PADBASEURL/$MATCHNAME" | sed 's/\.txt$/.html/'`

        sed -n "${PREVEMPTYLINE},${NEXTEMPTYLINE}p" $DUMPHERE | #
        cut -d ":" -f 2- | #
        pandoc -r markdown -w html | #
        tee             >> $HTMLOUT
        echo "<div class=\"srcurl\">\
              <a href=\"$MATCHURL\">→ </a></div>" >> $HTMLOUT
        fi

        LASTMATCH="$PREVEMPTYLINE"

  done )

  echo "</body></html>" >> $HTMLOUT
  echo "DONE. Your selection is in $HTMLOUT"

  else
       echo "No $KEYWORD found."
  fi 


# --------------------------------------------------------------------------- #
#  C L E A N   U P   A N D   E X I T
# --------------------------------------------------------------------------- #


exit 0;
