#!/usr/bin/env python
from __future__ import print_function
from argparse import ArgumentParser
from urllib2 import urlopen
from urllib import quote
import json, sys, os


urlpat = "https://farm{0[farm]}.staticflickr.com/{0[server]}/{0[id]}_{0[secret]}_{0[size]}.jpg"
search = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key={0[apikey]}&text={0[text]}&format=json&nojsoncallback=1"
p = ArgumentParser("search flickr")
p.add_argument("--apikey", default="29ba397bb6cc835860ec7ca7d8e3d90d", help="flickr API key")
p.add_argument("--termsurl", default="https://www.flickr.com/services/api/tos/", help="terms of use URL")
p.add_argument("--text", default="promiscuous pipelines", help="text to search for")
p.add_argument("--size", default="m", help="""size code, values: mstzb, default: m.
	Values: 
	s: small square 75x75,
	q: large square 150x150,
	t: thumbnail, 100 on longest side,
	m: small, 240 on longest side,
	n: small, 320 on longest side,
	-: medium, 500 on longest side,
	z: medium 640, 640 on longest side,
	c: medium 800, 800 on longest side,
	b: large, 1024 on longest side,
	h: large 1600, 1600 on longest side,
	k: large 2048, 2048 on longest side,
	o: original image, either a jpg, gif or png, depending on source format""")
p.add_argument("--download", action="store_true", help="download")
p.add_argument("--dir", default=".", help="directory to download to, default: .")
p.add_argument("--filename", default="{0[owner]}_{0[id]}_{0[size]}.jpg", help="pattern to use for name, default: {0[owner]}_{0[id]}_{0[size]}.jpg")
args = p.parse_args()

search_url = search.format({
	'apikey': args.apikey,
	'text' : quote(args.text)
	})
print (search_url, file=sys.stderr)
resp = json.load(urlopen(search_url))
for d in resp["photos"]["photo"]:
	d["size"] = args.size
	imgurl = urlpat.format(d)
	print (imgurl)
	if args.download:
		fname = os.path.join(args.dir, args.filename.format(d))
		print ("  saving to {0}...".format(fname), file=sys.stderr)
		fin = urlopen(imgurl)
		with open(fname, "w") as fout:
			while True:
				data = fin.read()
				if not data:
					break
				fout.write(data)
		fin.close()


