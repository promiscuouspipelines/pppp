#!/bin/bash

  SOURCESTXT="http://pad.constantvzw.org/p/pppp.list/export/txt"
  SOURCESURL="http://pad.constantvzw.org/p/pppp.list"
   IMAGES="images"
    AUDIO="audio"
    VIDEO="video"
     ELSE="var"

# --------------------------------------------------------------------------- #
# INTERACTIVE CHECKS (DISABLE IF YOU KNOW WHAT YOU'RE DOING ;))
# --------------------------------------------------------------------------- #
  function e() { echo "$*"; }

  clear;e ""

  e "   _                                            _                       "
  e "  /  _ || _  __|_o._  _  |\/| _._|_ _ ._o _.| _|__ ._                   "
  e "  \_(_)||(/_(_ |_|| |(_| |  |(_| |_(/_| |(_||  |(_)|                    "
  e "   –                  _|     –                  –                       "
  e "  |_).__ ._ _ o _ _ _     _ |_)o._  _ |o._  _  |_) |_ |o _ _._|_o _ ._  "
  e "  |  |(_)| | ||_>(_(_)|_|_> |  ||_)(/_||| |(/_ ||_||_)||(_(_| |_|(_)| | "
  e "                                |                                       "
  e "  -> $SOURCESURL"
  e
  read -p "  Do you want to proceed [y/n] " ANSWER
  if [ X$ANSWER != Xy ] ; then echo "  Sorry to bother you."
                               echo "  See you soon."
                               echo ""
                               exit 0; fi

# --------------------------------------------------------------------------- #
# GET THE MAIN LIST
# --------------------------------------------------------------------------- #

  IFHTTP=`echo $SOURCESTXT | grep "http.\?://" | wc -l`
  if [ $IFHTTP -ge 1 ]; then
       RESPONSE=`curl -s -o /dev/null -IL -w "%{http_code}" $SOURCESTXT`
       if [ $RESPONSE == '200' ]; then
            curl -k $SOURCESTXT -o sources.tmp > /dev/null 2>&1 
       else
            echo "  $SOURCESTXT not responding"
       fi
  else
            cp $SOURCESTXT sources.tmp > /dev/null 2>&1
  fi

# --------------------------------------------------------------------------- #
# DOWNLOAD THE STUFF
# --------------------------------------------------------------------------- #

  if [ -f sources.tmp ]; then e

  for THERE in `cat sources.tmp`
   do
       BASENAME=`echo $THERE | rev |      #
                 cut -d "/" -f 1   | rev` #
      EXTENSION=`echo $BASENAME | grep "\.[a-z0-9]*$" | #
                 rev | cut -d "." -f 1 | rev`
      if [ `echo $EXTENSION | wc -c` -gt 1 ]; then
            EXTENSION=".$EXTENSION"
      else
            EXTENSION=""
      fi 
      if [ `echo $EXTENSION | #
            egrep "\.jpg$|\.png$|\.tif$|\.gif$" | #
            wc -l` -gt 0 ]; then
            DUMPTO=$IMAGES
      elif [ `echo $EXTENSION | #
              egrep "\.mp3$|\.ogg$|\.wav$" | #
              wc -l` -gt 0 ]; then
              DUMPTO=$AUDIO
      elif [ `echo $EXTENSION | #
              egrep "\.mp4$|\.mov$|\.avi$" | #
              wc -l` -gt 0 ]; then
              DUMPTO=$VIDEO
      else 
            DUMPTO=$ELSE
      fi

             ID=`echo $THERE | md5sum | cut -c 1-4`
      CLEANNAME=`echo $BASENAME           | #
                 cut -d "." -f 1          | #
                 sed 's/[^a-zA-Z0-9_-]*//g'`
         TARGET=${CLEANNAME}_${ID}${EXTENSION}

      # TODO: $DUMPTO ACCORDING TO EXTENSION

      HERE=$DUMPTO/$TARGET
      if [ ! -f $HERE ]; then
       RESPONSE=`curl -s -o /dev/null -IL -w "%{http_code}" $THERE`
       if [ $RESPONSE == '200' ]; then
            echo "  Downloading $THERE -> $HERE"
            curl -k $THERE -o $HERE > /dev/null 2>&1
       else
            echo "! $THERE not found"
       fi
      else
            echo "  $HERE already downloaded"
      fi
  done
        rm sources.tmp 
  fi



  e""; exit 0;
