from __future__ import print_function
from argparse import ArgumentParser
import sys, cv2, json, os
from faces import get_faces_and_eyes

p = ArgumentParser("replace a face")
p.add_argument("--output", default="output.avi")
p.add_argument("--framerate", type=float, default=25, help="output frame rate")
p.add_argument("--frames", type=int, default=None, help="output only so many frames")
p.add_argument("--fourcc", default="mp4v", help="output format as fourcc value, values:XVID,MJPG,mp4v,XVID")
p.add_argument("--cascadexml", default="haarcascade_frontalface_default.xml", help="path to face detect xml default: haarcascades_frontalface_default.xml")
p.add_argument("--video", default="input.avi", help="input video")
p.add_argument("--face", default="face.jpg", help="input face image")
p.add_argument("--width", type=int, default=320, help="output width")
p.add_argument("--height", type=int, default=240, help="output height")
args = p.parse_args()

try:
    FRAME_COUNT = cv2.CAP_PROP_FRAME_COUNT
    FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
except AttributeError:
    FRAME_COUNT = cv2.cv.CV_CAP_PROP_FRAME_COUNT
    FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT

def get_faces (face_cascade, img, draw=False):
    ret = False
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    ar_faces=[]
    for (x,y,w,h) in faces:
        #print "face", (x, y, w, h)
        ar_faces.append([int(x), int(y), int(w), int(h)])
        #print type(x)
        ret = True
        if draw:
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    return ret, ar_faces

face_cascade = cv2.CascadeClassifier(args.cascadexml)
faceimg = cv2.imread(args.face)
try:
    fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
except AttributeError:
    fourcc = cv2.VideoWriter_fourcc(*args.fourcc)

src = cv2.VideoCapture(args.video)
owidth = int(src.get(FRAME_WIDTH))
oheight = int(src.get(FRAME_HEIGHT))

out = cv2.VideoWriter()
width = args.width or owidth
height = args.height or oheight
out.open(args.output, fourcc, args.framerate, (width, height))
count = 0
frameno = 1
while True:
    sys.stderr.write("\r{0}...".format(frameno))
    sys.stderr.flush()
    ret, frame = src.read()
    if frame == None:
        break
    det, faces = get_faces(face_cascade, frame)

    if len(faces):
        print (frameno, faces)
    for face in faces:
        x, y, w, h = face
        rfaceimg = cv2.resize(faceimg, (w, h))
        frame[y:y+h, x:x+w] = rfaceimg[0:h, 0:w]

    if width != owidth or height != oheight:
        frame = cv2.resize(frame, (width, height))
    out.write(frame)
    frameno += 1
    count += 1
    if args.frames and count >= args.frames:
        break

src.release()
out.release()


